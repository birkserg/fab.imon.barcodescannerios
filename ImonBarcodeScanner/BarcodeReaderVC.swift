//
//  BarcodeReaderVC.swift
//  ImonBarcodeScanner
//
//  Created by Sergej Birklin on 29/02/16.
//  Copyright © 2016 Sergej Birklin. All rights reserved.
//

import UIKit
import AVFoundation
import RSBarcodes_Swift

class BarcodeReaderVC: RSCodeReaderViewController {
    
    var dispatched = false
    
    @IBOutlet weak var cancelCaptureImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:Selector("cancelCaptureImageViewTapped:"))
        cancelCaptureImageView.userInteractionEnabled = true
        cancelCaptureImageView.addGestureRecognizer(tapGestureRecognizer)
        
        self.focusMarkLayer.strokeColor = UIColor.redColor().CGColor
        self.cornersLayer.strokeColor = UIColor.yellowColor().CGColor
        self.tapHandler = { point in print(point)}
        let types = NSMutableArray(array: self.output.availableMetadataObjectTypes)
        types.removeObject(AVMetadataObjectTypeQRCode)
        self.output.metadataObjectTypes = NSArray(array: types) as [AnyObject]
        
        for subview in self.view.subviews {
            self.view.bringSubviewToFront(subview)
        }
        
        self.barcodesHandler = { barcodes in
            if !self.dispatched {
                self.dispatched = true
                for barcode in barcodes {
                    //print("Barcode found: type=" + barcode.type + " value=" + barcode.stringValue)
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.goToWebVC(barcode.stringValue)
                    })
                }
            }
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        self.dispatched = false // reseting flag sothat user is able to scan again
        self.navigationController?.navigationBarHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.navigationBarHidden = false
    }
    
    func goToWebVC(barcode: String) {
        let webVC = self.navigationController!.viewControllers.first as! WebVC
        webVC.barcode = barcode
        self.navigationController!.popToRootViewControllerAnimated(true)
    }
    
    func cancelCaptureImageViewTapped(img: AnyObject)
    {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.navigationController?.popViewControllerAnimated(true)
        })
    }
    
}
