//
//  ChangeUrlTVC.swift
//  ImonBarcodeScanner
//
//  Created by Sergej Birklin on 09/03/16.
//  Copyright © 2016 Sergej Birklin. All rights reserved.
//

import UIKit

class ChangeUrlTVC: UITableViewController, UITextFieldDelegate {

    @IBOutlet weak var currentUrlLabel: UILabel!
    @IBOutlet weak var newUrlTextField: UITextField!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        currentUrlLabel.text = NSUserDefaults.standardUserDefaults().objectForKey("URL") as? String
        newUrlTextField.delegate = self
        newUrlTextField.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        newUrlTextField.resignFirstResponder()
        return true
    }

    @IBAction func okayButton(sender: UIButton) {
        let newUrl = newUrlTextField.text
        
        if newUrl?.characters.count > 0 {
            NSUserDefaults.standardUserDefaults().setValue(newUrl, forKey: "URL")
            defaults.synchronize()
            API.BASE_URL = newUrl!
            API.setScannedBarCode(newUrl!)
            showPositiveAlert("Bestätigt", message: "Die URL wurde geändert.")
        } else {
            showNegativeAlert("Fehler", message: "Die URL konnte nicht geändert werden.")
        }
    }

    @IBAction func resetButton(sender: UIButton) {
        let defaultURL = "https://skillworks-service.de:8443/imon"
        NSUserDefaults.standardUserDefaults().setValue(defaultURL, forKey: "URL")
        defaults.synchronize()
        API.BASE_URL = defaultURL
        API.setScannedBarCode(defaultURL)
        showPositiveAlert("Bestätigt", message: "Die URL wurde zurückgesetzt.")
    }
    
    func goToWebVC() {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func showPositiveAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { action in
            dispatch_async(dispatch_get_main_queue()) { () -> Void in
                self.goToWebVC()
            }
        }))
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func showNegativeAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
}
