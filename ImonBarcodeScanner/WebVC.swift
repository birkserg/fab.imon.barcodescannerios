//
//  WebVC.swift
//  ImonBarcodeScanner
//
//  Created by Sergej Birklin on 29/02/16.
//  Copyright © 2016 Sergej Birklin. All rights reserved.
//

import UIKit
import AVFoundation
import WebKit

class WebVC: UIViewController, UIWebViewDelegate {
    
    var allowScan = false
    var barcode = ""
    
    @IBOutlet weak var scanBarcodeButton: UIBarButtonItem!
    @IBAction func scanBarcodeButton(sender: UIBarButtonItem) {
        if allowScan == true {
            performSegueWithIdentifier("scanBarcode", sender: self)
        } else {
            checkCamera()
        }
    }
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        activityIndicator.hidesWhenStopped = true
        checkCamera()
        loadURLRequest("\(API.BASE_URL)/#/?app=ios")
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(true)
        if barcode != "" {
            loadURLRequest(API.SCANNEDBARCODE + barcode)
        } else {
            loadURLRequest(API.BASE_URL)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - WebView
    func loadURLRequest(address: String) {
        let siteURL = NSURL(string: address)
        let request = NSURLRequest(URL: siteURL!)
        webView.loadRequest(request)
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        activityIndicator.stopAnimating()
    }
    
    // MARK: - Camera check
    func checkCamera() {
        let authStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        switch authStatus {
        case .Authorized: allowScan = true
        case .Denied: alertToEncourageCameraAccessInitially()
        case .NotDetermined: alertPromptToAllowCameraAccessViaSetting()
        default: alertToEncourageCameraAccessInitially()
        }
    }
    
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Camera access required for Barcode Scanning",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: nil))
        alert.addAction(UIAlertAction(title: "Allow Camera", style: .Cancel, handler: { (alert) -> Void in
            UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
        }))
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func alertPromptToAllowCameraAccessViaSetting() {
        let alert = UIAlertController(
            title: "IMPORTANT",
            message: "Please allow camera access for Barcode Scanning",
            preferredStyle: UIAlertControllerStyle.Alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .Cancel) { alert in
            if AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo).count > 0 {
                AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo) { granted in
                    dispatch_async(dispatch_get_main_queue()) {
                        self.checkCamera() } }
            }
            }
        )
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
}

