//
//  RestService.swift
//  ImonBarcodeScanner
//
//  Created by Sergej Birklin on 29/02/16.
//  Copyright © 2016 Sergej Birklin. All rights reserved.
//

import Foundation
import Alamofire

public class RestService {
    
    static func getRequest(withURL: String, success: ((AnyObject) -> Void)!, failure: ((NSError, AnyObject?) -> Void)!) {
        Alamofire.request(.GET, withURL).responseString { (response) -> Void in
            print(response)
        }
    }
}

struct API {
    // old
    //static let BASE_URL = "http://192.168.3.113:8080"
    //static let ScannedBarCode = "\(BASE_URL)/?#/ack?id="
    
    static var BASE_URL = NSUserDefaults.standardUserDefaults().objectForKey("URL") as! String
    static var SCANNEDBARCODE = "\(BASE_URL)/#/ack?app=ios&id="
    
    static func setScannedBarCode(baseUrl: String) {
        SCANNEDBARCODE = "\(BASE_URL)/#/ack?app=ios&id="
    }

    // new
    //static let BASE_URL = NSUserDefaults.standardUserDefaults().objectForKey("URL") as! String
    //static let ScannedBarCode = "\(BASE_URL)#/ack?app=ios&id="
}